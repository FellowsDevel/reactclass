import React from 'react';

const ValidationComponent = (props) => {

  let textValue = props.strLen <= 5 ? "Text too Short" : "Text long enough"
  return (
    <div>
      <p>{textValue}</p>
      <p>{props.strLen}</p>
    </div>
  );
}

export default ValidationComponent;