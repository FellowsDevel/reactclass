import React from 'react';
import classes from './Cockpit.css';
import AuthContext from '../../context/auth-context';

const cockpit = props => {

  const assignedClasses = [];
  let btnClass = '';

  if (props.showPersons) {
    btnClass = classes.red;
  }

  if (props.personsLenght <= 2) {
    assignedClasses.push(classes.red);
  }
  if (props.personsLenght <= 1) {
    assignedClasses.push(classes.bold);
  }

  return (
    <div className={classes.Cockpit}>
      <h1>{props.title}</h1>
      <p className={assignedClasses.join(' ')}>This is really working!</p>
      <button
        className={btnClass}
        onClick={props.clicked}>Toggle Persons</button>
      <AuthContext.Consumer>
        {context => <button onClick={context.login}>Log In</button>}
      </AuthContext.Consumer>
    </div>
  );
}

/*
 * Utilizando react como função (em vez de classe), é necessário utilizar 
 * o consumer para acessar o contexto em vez de usar o contextType
 */

export default React.memo(cockpit);
// Utilizando o React.memo, só haverá renderização caso algum parametro seja alterado
