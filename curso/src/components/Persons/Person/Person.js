import React, { Component } from 'react';
import PropTypes from 'prop-types';

import classes from './Person.css';
import Auxiliary from '../../../hoc/Auxiliary';
import withClass from '../../../hoc/withClass';
import AuthContext from '../../../context/auth-context';

class Person extends Component {

  static contextType = AuthContext;

  componentDidMount() {
    console.log('[Person.js] componentDidMount context', this.context.authenticated);
  }

  render() {
    console.log('[Person.js] render');
    return (
      <Auxiliary>
        {this.context.authenticated ? <p>Authenticated</p> : <p>Please Login</p>}
        <p onClick={this.props.click}>
          My name is {this.props.name} and my age is {this.props.age} years
          old
          </p>
        <p>{this.props.children}</p>
        <input
          type='text'
          onChange={this.props.changed}
          value={this.props.name}
        />
      </Auxiliary>
    );
  }
}

/**
 * Faz a amarração dos tipos dos parametros para exibir warnings se
 * os mesmos não forem respeitados
 */
Person.propTypes = {
  click: PropTypes.func,
  name: PropTypes.string,
  age: PropTypes.number,
  changed: PropTypes.func
};

export default withClass(Person, classes.Person);
