// Default Component implementation
import React, { Component } from 'react';
import Persons from '../components/Persons/Persons';
import Cockpit from '../components/Cockpit/Cockpit';
import withClass from '../hoc/withClass';
import Auxiliary from '../hoc/Auxiliary';
import classes from './App.css';
import AuthContext from '../context/auth-context';

class App extends Component {
  constructor(props) {
    super(props);
    console.log('[App.js] constructor');

    // we can initialize State here using this
    this.state = {
      persons: [
        { id: 1, name: 'Nome 1', age: 22 },
        { id: 2, name: 'Nome 2', age: 23 },
        { id: 3, name: 'Nome 3', age: 24 }
      ],
      otherState: 'some other value',
      showPersons: true,
      changeCounter: 0,
      authenticated: false
    };
  }

  static getDerivedStateFromProps(props, state) {
    console.log('[App.js] getDerivedStateFromProps', props);
    return state;
  }

  deletePersonHandler = index => {
    // Como em JS Array é por referência,
    // aqui estamos alterando o objeto dentro do State do React o que pode
    // causar comportamentos imprevisíveis
    /*
    const list = this.state.persons;
    list.splice(index, 1);
    this.setState({ persons: list });
    */

    // melhor criar uma cópia do array antes desta forma:
    const list = [...this.state.persons]; // utilizar o Spread (...) para criar um novo array
    // moodo antigo de criar um novo array a partir de um array
    // const list = this.state.persons.slice();
    list.splice(index, 1);
    this.setState({ persons: list });
  };

  nameChangedHandler = (event, id) => {
    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id;
    });

    const person = { ...this.state.persons[personIndex] }; // Spread o item em um novo item
    // outra alternativa seria:
    // const person = Object.assign({}, this.state.persons[personIndex]);
    person.name = event.target.value;

    const persons = [...this.state.persons];
    persons[personIndex] = person;

    // this.setState({ persons: persons, changeCounter: this.state.changeCounter + 1 });
    this.setState((prevState, props) => {
      return { persons: persons, changeCounter: prevState.changeCounter + 1 };
    });
  };

  togglePersonsHandler = () => {
    const doesShow = this.state.showPersons;
    this.setState({ showPersons: !doesShow });
  };

  componentDidMount() {
    console.log('[App.js] componentDidMount');
  }

  loginHandler = () => {
    this.setState({ authenticated: true });
  };

  logout() {
    this.setState({ authenticated: false });
  };

  render() {
    console.log('[App.js] render');

    let persons = null;

    if (this.state.showPersons) {
      persons = (
        <div>
          <Persons
            persons={this.state.persons}
            clicked={this.deletePersonHandler}
            changed={this.nameChangedHandler}
            isAuthenticated={this.state.authenticated}
          />
        </div>
      );
    }

    return (
      <Auxiliary>
        <AuthContext.Provider value={{
          authenticated: this.state.authenticated,
          login: this.loginHandler
        }}>
          <Cockpit
            title={this.props.appTitle}
            showPersons={this.state.showPersons}
            personsLenght={this.state.persons.length}
            clicked={this.togglePersonsHandler}
          />
          {persons}
        </AuthContext.Provider>
      </Auxiliary>
    );
  }
}

export default withClass(App, classes.App);
