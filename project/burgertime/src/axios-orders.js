import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://react-my-burger-2bc3e.firebaseio.com/'
});

export default instance;
